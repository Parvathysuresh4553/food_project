## Name: Parvathy Suresh

## Food Project

This is a test project of a food website centre to demonstrate using Intern with Angular 4+.This example uses a set of common anchor links which redirects to each facility in the centre.It also contains some code for a customer registration and feedback.It has the details of five famous reciepies of food and the diet that should be followed for a balanced diet.The pictures and details of each food is described in detailed.There are atleat 2 recipies for a food item.

---

## Reason for MIT License

You canfind the MIT License in https://bitbucket.org/Parvathysuresh4553/food_project/src/master/MIT-LICENSE.txt

The reason to use this license is that it will allow any user to use this code.The MIT License is a permissive free software license so the user can download the code modify it and publish it.The MIT license also permits reuse within proprietary software, provided that all copies of the licensed software include a copy of the MIT License terms and the copyright notice.This allows the user to learn from this code and create a better version of this.


## Deployment Instructions (How to run it locally)

Start setting up this project by installing Visual Studio Code.

1. Download or clone the repository to your local machine: git clone https://Parvathysuresh4553@bitbucket.org/Parvathysuresh4553/food_project.git
2. Run npm install inside the downloaded/cloned folder: $ npm install
3. Start the dev server by running the command below: $ npm test
4. Navigate to http://localhost:4200/. The app will automatically reload if you change any of the source files
5. Perform the operations in the website.
6. Go back to the **Source** page.

---
## User Instructions to build the project in Visual Studio

1. In Solution Explorer, choose or open the solution.
2. On the menu bar, choose Build, and then choose one of the following commands:
3. Choose Build or Build Solution, or press Ctrl+Shift+B, to compile only those project files and components that have changed since the most recent build.
4. Choose Rebuild Solution to "clean" the solution and then build all project files and components.

Choose Clean Solution to delete any intermediate and output files. With only the project and component files left, new instances of the intermediate and output files can then be built.
## Documentation

The complete Angular documentation can be found at https://angular.io/cli.

---

## About us

If you want to know more about the website please go through the link https://www.foodbasics.ca/.

